all : instantiate-mongodb build-go test-go build-go-docker-image upload-go-docker-image run-minikube deploy-go-kubernetes

tag:=$(shell git tag)

.PHONY: instantiate-mongodb
build:
	docker run -itd --name mongodb --network container-network -p 27017:27017 -v /tmp/mongodb:/data/db ruanbekker/mongodb

.PHONY: build-go
build:
	GOOS=linux go build -o $(tag) -v ./main.go

.PHONY: test-go
build:
	go test -run TestVarible config_test.go

.PHONY: build-go-docker-image
build-docker-image:
	docker build -t "elliegm/restconf:v1" -f docker/Dockerfile .

.PHONY: upload-go-docker-image
upload-docker-image:
	docker push elliegm/restconf:v1

.PHONY: run-minikube
run-minikube:
	minikube start --memory 5120 --cpus=4

.PHONY: deploy-go-kubernetes
deploy-kubernetes:
	kubectl apply -f kubernetes/deployment.yaml