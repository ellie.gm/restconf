package config

import (
	"github.com/sirupsen/logrus"
	"github.com/tkanos/gonfig"
	"os"
)

type Config struct {
	Url        string
	Username   string
	Password   string
}

func Variable()(string, string, string) {

	// Set log output
	File, err := os.OpenFile("log/restconf.log", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err !=nil {
		logrus.Print(err.Error())
	}
	logrus.SetFormatter(&logrus.TextFormatter{})
	logrus.SetOutput(File)

	// reade config file and write it to the struct
	configuration := Config{}
	er := gonfig.GetConf("config/config.dev.json", &configuration)
	if er != nil {
		logrus.Print(er)
	}

	Url := configuration.Url
	Username := configuration.Url
	Password := configuration.Password
	return Url, Username, Password
}