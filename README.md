## RESTCONF
This repository contains RESTCONF APIs for configuring a RESTCONF Agent.

#### Installation guide
Application compilation, Docker image creation and application deployment on Kubernetes is done via Makefile:
```
make -f Makefile -d
```