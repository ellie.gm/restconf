package main

import (
	"RESTCONF/config"
	"bytes"
	"crypto/tls"
	"golang.org/x/crypto/bcrypt"
	"encoding/gob"
	"fmt"
	"github.com/gorilla/securecookie"
	"github.com/sirupsen/logrus"
	"html/template"
	"io/ioutil"
	"net/http"
	"os"
	"github.com/gorilla/sessions"
)

func main() {

	// Add local CSS file
	http.Handle("/static/", http.StripPrefix("/static/", http.FileServer(http.Dir("static"))))

	// HTTP handlers for each application route
	http.HandleFunc("/", signin)
	http.HandleFunc("/signup", signup)
	http.HandleFunc("/admin", homepage)
	http.HandleFunc("/interface/", int)
	http.HandleFunc("/interface/config", intConfig)
	http.HandleFunc("/route/", route)
	http.HandleFunc("/route/config", routConfig)
	http.HandleFunc("/logout", logout)


	// Listen to the incoming request to port 80
	http.ListenAndServe(":8080", nil)

}

// Store users cookies
type User struct {
	Username      string
	Authenticated bool
}

var cookieStore *sessions.CookieStore

var temp *template.Template

func init (){
	// generate cookie keys
	authKey := securecookie.GenerateRandomKey(64)
	encryptionKey := securecookie.GenerateRandomKey(32)

	cookieStore = sessions.NewCookieStore(
		authKey,
		encryptionKey,
	)

	cookieStore.Options = &sessions.Options{
		MaxAge:   60 * 15,
		HttpOnly: true,
	}

	gob.Register(User{})

	// Pars html templates
	temp = template.Must(template.ParseGlob("templates/*.html"))
}

func signin(write http.ResponseWriter, read *http.Request)  {
	
	// Set log output
	File, er := os.OpenFile("log/restconf.log", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if er !=nil {
		fmt.Println(er.Error())
	}
	logrus.SetFormatter(&logrus.TextFormatter{})
	logrus.SetOutput(File)

	// Read cookies from the request
	session, er := cookieStore.Get(read, "cookie-name")
	
	if er !=nil {
		logrus.Print(er)
	}

	if read.Method == "POST" {
		read.ParseForm()
		username:=read.FormValue("username")
		password:=read.FormValue("password")
		hashedPassword, er := bcrypt.GenerateFromPassword([]byte(password), 8)

		if er !=nil {
			logrus.Print(er)
		}

		// Check password and username values
		usernameCheck:=emptyCheck(username)
		passwordCheck:=emptyCheck(password)

		if usernameCheck || passwordCheck {
			fmt.Fprint(write, "Please do not leave any field empty")
		}

		dbUsername := "ellie"
		dbPassword, er := bcrypt.GenerateFromPassword([]byte("123"), 8)

		if username == dbUsername && hashedPassword == dbPassword {
			user := &User{
				Username:      username,
				Authenticated: true,
			}
			session.Values["user"] = user
			er = session.Save(read, write)
			if er != nil {
				http.Error(write, er.Error(), http.StatusInternalServerError)
				logrus.Print(er)
				return
			}

			http.Redirect(write, read, "/admin", http.StatusFound)
		} else {
			er = session.Save(read, write)
			if er != nil {
				http.Error(write, er.Error(), http.StatusInternalServerError)
				logrus.Print(er)
				return
			}
			fmt.Fprint(write, "Incorrect usernme or password")
		}
	} else {
		temp.ExecuteTemplate(write, "signin.html", nil)
	}
}

func signup(write http.ResponseWriter, read *http.Request){

	// Set log output
	File, er := os.OpenFile("log/restconf.log", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if er !=nil {
		fmt.Println(er)
	}
	logrus.SetFormatter(&logrus.TextFormatter{})
	logrus.SetOutput(File)
	
	if read.Method == "POST" {
		read.ParseForm()
		username:=read.FormValue("username")
		email:=read.FormValue("email")
		password:=read.FormValue("password")
		vPassword:=read.FormValue("vPassword")

		usernameCheck:=emptyCheck(username)
		emailCheck:=emptyCheck(email)
		passwordCheck:=emptyCheck(password)
		vPasswordCheck:=emptyCheck(vPassword)

		if usernameCheck || emailCheck || passwordCheck || vPasswordCheck {
			fmt.Fprint(write, "Please fill all fields! \n")
		}

		if password == vPassword {

			hashedPassword, er := bcrypt.GenerateFromPassword([]byte(password), 8)
			if er !=nil {
				logrus.Print(er)
			}

			http.Redirect(write, read, "/", http.StatusFound)
		}else {
			fmt.Fprint(write, "Passwords does not match! \n")
		}

	} else {
		temp.ExecuteTemplate(write, "signup.html", nil)
	}
}

func logout(write http.ResponseWriter, read *http.Request) {
	//temp.ExecuteTemplate(write, "logout.html", nil)

	// Set log output
	File, er := os.OpenFile("log/restconf.log", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if er !=nil {
		fmt.Println(er.Error())
	}
	logrus.SetFormatter(&logrus.TextFormatter{})
	logrus.SetOutput(File)

	// Read cookies from the request
	session, er := cookieStore.Get(read, "cookie-name")
	if er != nil {
		http.Error(write, er.Error(), http.StatusInternalServerError)
		logrus.Print(er)
		return
	}

	session.Values["user"] = User{}
	session.Options.MaxAge = -1
	er = session.Save(read, write)
	if er != nil {
		http.Error(write, er.Error(), http.StatusInternalServerError)
		logrus.Print(er)
		return
	}
	http.Redirect(write, read, "/", http.StatusFound)

}

func emptyCheck(data string) bool{
	if len(data) == 0 {
		return true
	}else{
		return false
	}

}

func getUser(s *sessions.Session) User {
	val := s.Values["user"]
	var user = User{}
	user, ok := val.(User)
	if !ok {
		return User{Authenticated: false}
	}
	return user
}

func homepage(write http.ResponseWriter, read *http.Request) {
	// Set log output
	File, err := os.OpenFile("log/restconf.log", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err !=nil {
		fmt.Println(err.Error())
	}
	logrus.SetFormatter(&logrus.TextFormatter{})
	logrus.SetOutput(File)

	// Read cookies from the request
	session, err := cookieStore.Get(read, "cookie-name")
	if err != nil {
		http.Redirect(write, read, "/", http.StatusFound)
		logrus.Print(err)
		return
	}

	user := getUser(session)

	if auth := user.Authenticated; !auth {
		http.Redirect(write, read, "/", http.StatusFound)
		err = session.Save(read, write)
		if err != nil {
			http.Error(write, err.Error(), http.StatusInternalServerError)
			logrus.Print(err)
			return
		}
		return
	}
	temp.ExecuteTemplate(write, "index.html", user.Username)
}

func int(write http.ResponseWriter, read *http.Request)  {

	// Set log output
	File, err := os.OpenFile("log/restconf.log", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err !=nil {
		fmt.Println(err.Error())
	}
	logrus.SetFormatter(&logrus.TextFormatter{})
	logrus.SetOutput(File)

	// Read cookies from the request
	session, err := cookieStore.Get(read, "cookie-name")
	if err != nil {
		http.Redirect(write, read, "/", http.StatusFound)
		logrus.Print(err)
		return
	}

	user := getUser(session)

	if auth := user.Authenticated; !auth {
		http.Redirect(write, read, "/", http.StatusFound)
		err = session.Save(read, write)
		if err != nil {
			http.Error(write, err.Error(), http.StatusInternalServerError)
			logrus.Print(err)
			return
		}
		return
	}
	temp.ExecuteTemplate(write, "interface.html", user.Username)
}

func route(write http.ResponseWriter, read *http.Request) {

	// Set log output
	File, err := os.OpenFile("log/restconf.log", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err !=nil {
		fmt.Println(err.Error())
	}
	logrus.SetFormatter(&logrus.TextFormatter{})
	logrus.SetOutput(File)

	// Read cookies from the request
	session, err := cookieStore.Get(read, "cookie-name")
	if err != nil {
		http.Redirect(write, read, "/", http.StatusFound)
		logrus.Print(err)
		return
	}

	user := getUser(session)

	if auth := user.Authenticated; !auth {
		http.Redirect(write, read, "/", http.StatusFound)
		err = session.Save(read, write)
		if err != nil {
			http.Error(write, err.Error(), http.StatusInternalServerError)
			logrus.Print(err)
			return
		}
		return
	}
	temp.ExecuteTemplate(write, "routing.html", user.Username)
}


func intConfig(write http.ResponseWriter, read *http.Request)  {

	// Set log output
	File, er := os.OpenFile("log/restconf.log", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if er !=nil {
		fmt.Println(er)
	}
	logrus.SetFormatter(&logrus.TextFormatter{})
	logrus.SetOutput(File)

	// Ignore certificate verification
	certificateIgnore := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}
	client := &http.Client{Transport:certificateIgnore}


	intName := read.FormValue("intName")

	agentAddress, username, password := config.Variable()
	//username := config.Variable()
	//password := config.Variable()
	formAction := read.FormValue("action")
	loopbackConfig := read.FormValue("loopbackConfig")
	loopbackConfigs := []byte(loopbackConfig)


	if formAction=="addLoopback"{
		url := agentAddress+":9443/restconf/data/ietf-interfaces:interfaces"
		logrus.Print(" POST ", url)
		request, er := http.NewRequest("POST", url, bytes.NewBuffer(loopbackConfigs))
		if er != nil {
			logrus.Print(er)
		}
		request.SetBasicAuth(username, password)
		request.Header.Add("Content-Type", "application/yang-data+json")
		request.Header.Add("Accept", "application/yang-data+json")

		response, er := client.Do(request)
		if er != nil {
			logrus.Print(er)
		}

		responseBody, er := ioutil.ReadAll(response.Body)
		if er != nil {
			logrus.Print(er)
		}
		defer response.Body.Close()

		logrus.Print(write, string(responseBody))
	}

	if formAction=="editLoopback"{
		url := agentAddress+":9443/restconf/data/ietf-interfaces:interfaces/interface="+intName
		request, er := http.NewRequest("PUT", url, bytes.NewBuffer(loopbackConfigs))
		if er != nil {
			logrus.Println(er)
		}
		request.SetBasicAuth(username, password)
		request.Header.Add("Content-Type", "application/yang-data+json")
		request.Header.Add("Accept", "application/yang-data+json")

		response, er := client.Do(request)
		if er != nil {
			logrus.Print(er)
		}

		responseBody, er := ioutil.ReadAll(response.Body)
		if er != nil {
			logrus.Print(er)
		}
		defer response.Body.Close()

		logrus.Print(write, string(responseBody))
	}

	if formAction=="delLoopback"{
		url := agentAddress+":9443/restconf/data/ietf-interfaces:interfaces/interface="+intName
		request, er := http.NewRequest("DELETE", url, bytes.NewBuffer(loopbackConfigs))
		if er != nil {
			logrus.Print(er)
		}
		request.SetBasicAuth(username, password)
		request.Header.Add("Content-Type", "application/yang-data+json")
		request.Header.Add("Accept", "application/yang-data+json")

		response, er := client.Do(request)
		if er != nil {
			logrus.Print(er)
		}

		responseBody, er := ioutil.ReadAll(response.Body)
		if er != nil {
			fmt.Print(er)
		}
		defer response.Body.Close()

		logrus.Print(write, string(responseBody))
	}

	if formAction=="editEthernet"{
		url := agentAddress+":9443/restconf/data/ietf-interfaces:interfaces/interface="+intName
		request, er := http.NewRequest("PUT", url, bytes.NewBuffer(loopbackConfigs))
		if er != nil {
			logrus.Print(er)
		}
		request.SetBasicAuth(username, password)
		request.Header.Add("Content-Type", "application/yang-data+json")
		request.Header.Add("Accept", "application/yang-data+json")

		response, er := client.Do(request)
		if er != nil {
			logrus.Print(er)
		}

		responseBody, er := ioutil.ReadAll(response.Body)
		if er != nil {
			logrus.Print(er)
		}
		defer response.Body.Close()

		logrus.Print(write, string(responseBody))
	}
}


func routConfig(write http.ResponseWriter, read *http.Request) {

	// Set log output
	File, er := os.OpenFile("log/restconf.log", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if er !=nil {
		fmt.Println(er.Error())
	}
	logrus.SetFormatter(&logrus.TextFormatter{})
	logrus.SetOutput(File)

	// Ignore certificate verification
	certificateIgnore := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}
	client := &http.Client{Transport: certificateIgnore}

	ospfConfig := read.FormValue("ospfConfig")
	ospfConfigs := []byte(ospfConfig)
	pID := read.FormValue("pID")
	formAction := read.FormValue("action")
	agentAddress, username, password := config.Variable()


	if formAction == "addOspf" {
		url := agentAddress + ":9443/restconf/data/Cisco-IOS-XE-native:native/router"
		request, er := http.NewRequest("POST", url, bytes.NewBuffer(ospfConfigs))
		if er != nil {
			logrus.Print(er)
		}
		request.SetBasicAuth(username, password)
		request.Header.Add("Content-Type", "application/yang-data+json")
		request.Header.Add("Accept", "application/yang-data+json")

		response, er := client.Do(request)
		if er != nil {
			logrus.Print(er)
		}

		responseBody, er := ioutil.ReadAll(response.Body)
		if er != nil {
			logrus.Print(er)
		}
		defer response.Body.Close()

		logrus.Print(write, string(responseBody))
	}

	if formAction == "editOspf" {
		url := agentAddress + ":9443/restconf/data/Cisco-IOS-XE-native:native/router/ospf="+pID
		request, er := http.NewRequest("PUT", url, bytes.NewBuffer(ospfConfigs))
		if er != nil {
			logrus.Print(er)
		}
		request.SetBasicAuth(username, password)
		request.Header.Add("Content-Type", "application/yang-data+json")
		request.Header.Add("Accept", "application/yang-data+json")

		response, er := client.Do(request)
		if er != nil {
			logrus.Print(er)
		}

		responseBody, er := ioutil.ReadAll(response.Body)
		if er != nil {
			logrus.Print(er)
		}
		defer response.Body.Close()

		logrus.Print(write, string(responseBody))
	}

	if formAction == "delOspf" {
		url := agentAddress + ":9443/restconf/data/Cisco-IOS-XE-native:native/router/ospf="+pID
		request, er := http.NewRequest("DELETE", url, bytes.NewBuffer(ospfConfigs))
		if er != nil {
			logrus.Print(er)
		}
		request.SetBasicAuth(username, password)
		request.Header.Add("Content-Type", "application/yang-data+json")
		request.Header.Add("Accept", "application/yang-data+json")

		response, er := client.Do(request)
		if er != nil {
			logrus.Print(er)
		}

		responseBody, er := ioutil.ReadAll(response.Body)
		if er != nil {
			logrus.Print(er)
		}
		defer response.Body.Close()

		logrus.Print(write, string(responseBody))
	}
}









